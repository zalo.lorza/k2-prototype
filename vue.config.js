const path = require('path')
const webpack = require('webpack')

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        '@js': path.resolve(__dirname, 'src/js'),
        '@css': path.resolve(__dirname, 'src/css'),
        '@components': path.resolve(__dirname, 'src/components'),
        '@views': path.resolve(__dirname, 'src/views'),
        '@util': path.resolve(__dirname, 'src/js/util'),
        '@store': path.resolve(__dirname, 'src/js/store'),
        '@api': path.resolve(__dirname, 'src/js/api'),
        '@router': path.resolve(__dirname, 'src/js/router')
      },
    },
    plugins: [
      new webpack.ProvidePlugin({
        log: ['@util/log', 'default'],
        util: ['@util', 'default']
      })
    ]
  }
}
