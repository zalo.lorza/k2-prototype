import Vue from 'vue'
import '@js/config/vue-tailwind'
import '@js/config/vue-konva'
import '@components/icons/icons'
import '@api'
import '@css/main.css'

Vue.config.productionTip = false

log.milestone('K2Customizer ready')