import QuantitySelector from '@components/quantity-selector/quantity-selector.vue'

/**
 * Editor footer view (bottom of the modal)
 */

export default {
  components: { QuantitySelector },
  computed: {
    actions(){
      return [
        { label: 'Save template', action: this.$api.save },
        { label: 'Add To Cart', action: this.$api.addToCart }
      ]
    }
  }
}