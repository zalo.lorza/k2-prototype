import KonvaCanvas from '@components/konva-canvas/konva-canvas.vue'
import NavNextPrev from '@components/nav-next-prev/nav-next-prev.vue'
/**
 * Editor canvas view (left panel on desktop)
 */

export default {
  components: { KonvaCanvas, NavNextPrev },
  props: ['border']
}