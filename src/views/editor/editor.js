/**
 * Editor View
 */

export default {
  computed: {
    paddingBottomCol(){
      return this.$store.getters['editor/wrapperProportion']+'%'
    }
  }
}