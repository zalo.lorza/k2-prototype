import Modal from '@components/modal/modal.vue'
import ModalNextPrev from '@components/modal-next-prev/modal-next-prev.vue'

/**
 * This is the main app wrapper
 */
export default {
  components: { 
    Modal, 
    ModalNextPrev
  },
  computed: {
    title(){
      return this.$route.meta.title ? this.parse(this.$route.meta.title) : 'Customizer'
    },
    isModal(){
      return typeof this.$route.meta.modal === 'undefined' || this.$route.meta.modal
    },
    hasFooter(){
      return this.$route.matched[0].components.footer
    },
    hasNextPrev(){
      return this.$route.meta.modalHasNextPrev
    }
  },
  methods:{
    parse(string){
      return string.replace(/{{.*?}}/g, match => {
        var expression = match.slice(2, -2)
        return function() { return eval(expression); }.call(this)
      })
    }
  },
  watch: {
    '$store.state.editor.items'(){
      this.$api.snapshot()
    }
  }
}