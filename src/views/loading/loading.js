import Loading from '@components/loading/loading.vue'

/**
 * Loading View (no modal)
 */

export default {
  components: { Loading }
}