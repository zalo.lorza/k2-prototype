import state from '@store/history/state'
import getters from '@store/history/getters'
import actions from '@store/history/actions'
import mutations from '@store/history/mutations'

/**
 * Here, we simply aggregate the definitions laid
 * out in each of the state, getter, action and
 * mutation files within this Vuex module.
 */
export default () => {
  return {
    namespaced: true,
    state: state(),
    getters,
    actions,
    mutations
  }
}
