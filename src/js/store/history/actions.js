/**
 * @info
 *
 * *The "store/history" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 *
 * *This file*
 * This file outlines "actions". Actions are functions that
 * include logic that, when ready, commits a mutation to the
 * application state. Whereas mutations must be synchronous,
 * actions return a promise and can include asynchronous
 * logical sequences.
 */

import api from '@api'
const { formatDate } = util

export default {
  /** 
   * Goes back one snapshot in history 
   * Moves the pointer without destroying/modifying history timelime
   * 
   * @wip
   */
  back: () => {
    
  },

  /** 
   * Goes forward one snapshot in history 
   * Moves the pointer without destroying/modifying history timelime
   * 
   * @wip
   */
  forward: () => {
    
  },

  /** 
   * Loads current snapshot to store state
   * 
   * @wip this will be used with back/forward methods 
   * and, maybe, when loading saved data from the server
   */
  loadSnapshot: () => {
    
  },

  /** 
   * Creates new snapshot
   * - If current data is equal as previous snapshot, new snapshot won't be created
   * - This method modifies histroy timeline and destroys next snapshots if pointer 
   *   is not placed on last available snapshot
   * 
   * @wip add destroy mechanism of future snapshots if pointer is placed in the past 
   * (this will be requiered when implemented back/forward/load methods)
   * 
   * @param {Integer} delay [default: 100] - debouncing time in ms 
   */

  newSnapshot: ({getters, commit, rootGetters}, delay = 100) => {
    if(delay === null) delay = 100
    clearTimeout(window.K2HistorySnapshotTimeout)
    window.K2HistorySnapshotTimeout = setTimeout(() => {
      log.msg('History: try new snapshot')
      const newSnapshot = rootGetters['editor/dataToSave']
      if(JSON.stringify(getters.currentSnapshot, 0) === JSON.stringify(newSnapshot, 0)) return
      if(getters.currentSnapshot) commit('SET_UNSAVED_CHANGES')
      commit('ADD_SNAPSHOT', newSnapshot)
      log.msg('History: added new snapshot')
    }, delay)
  },

  /** 
   * Save current history snapshot in order to be retrieved later.
   * This also sets needsToSave to false, since user has safely saved its data
   * 
   * @wip right now this is a placeholder method that saves a local JSON file.
   * In the future this will be store data to a remote DB.
   * !Suggestion: compressing JSON data before storing it to the server
   * !Suggestion: consider autosaving after many new snapshots or using time interval 
   * (keep in mind performance implications)
   * 
   * @param {Integer} delay [default: 100] - debouncing time in ms
   * 
   * @fires loading
   */

  save: ({commit, state, getters, dispatch}) => {
    if(!state.hasUnsavedChanges) {
      alert('Nothing to save!')
      log.warn('Nothing to save!')
      return;
    }
    api.emit('loading', 'Saving')
    dispatch('newSnapshot', 0)

    /** @wip */
    setTimeout(() => {
      const name = `k2-customizer-${formatDate(null, 'mm-dd-yy')}.json`
      var downloadNode = document.createElement('a')
      downloadNode.setAttribute("href", "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(getters.currentSnapshot)))
      downloadNode.setAttribute("download", name);
      document.body.appendChild(downloadNode); // required for firefox
      downloadNode.click();
      downloadNode.remove();
      commit('SET_UNSAVED_CHANGES', false)
      log.milestone(`Saved ${name} file`)
      log.obj(getters.currentSnapshot)
      setTimeout(() => {
        api.emit('loading', false)
      }, 500)
    }, 1)
  }
}
