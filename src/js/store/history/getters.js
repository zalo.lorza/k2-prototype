/**
 * @info
 *
 * *The "store/history" directory*
 * This "store" directory declares each of the Vuex

 * *This file*
 * This file outlines "getters". Getters are methods
 * that perform modifications to the state value prior
 * to returning the value to the consuming component.
 */

export default {

  /** 
   * Retrieves current history snapshot
   * 
   * @returns {Object} data snapshot
   */

  currentSnapshot: state => {
    if(state.currentIndex === -1) return null
    return state.snapshots[state.currentIndex]
  }
}
