/**
 * @info
 *
 * *The "store/history" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 *
 * *This file*
 * This file outlines "state". This is the starting snapshot
 * of the modules underlying state. As the user interacts with
 * the components tied into this Vuex module, this state is modified
 * away from these default values.
 */

/** 
* @property {Array}    snapshots          [default: []]    Array of data snapshots (history milestones) 
* @property {Integer}  currentIndex       [default: -1]    Current history pointer (current snapshot index). Used for history navigation
* @property {Boolean}  hasUnsavedChanges  [default: false] User has unsaved changes that can be lost if closing or resetting the customizer
*/

export default () => {
  return {
    snapshots: [],
    currentIndex: -1,
    hasUnsavedChanges: false
  }
}
