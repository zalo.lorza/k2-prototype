/**
 * @info
 *
 * *The "store/history" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind history functionality.
 *
 * *This file*
 * This file outlines "mutations". Mutations are intended
 * to be as simple as possible. All modifications to the state
 * object should happen inside a mutation function. Each
 * of these functions must be synchronous. There should be nothing
 * that has a time delay, such as an AJAX request or a setTimeout.
 */

import resetState from '@store/history/state'
const { clone } = util

export default {
  /** 
   * Resets state to default values (use to unload data)
   */

  RESET: state => {
    Object.assign(state, resetState(state.location))
  },

  /** 
   * Adds a new snapshot (data point) to history and 
   * moves the index pointer to current snapshot
   * 
   * @param {Object} snapshot New data history point
   */

  ADD_SNAPSHOT: (state, snapshot) => {
    state.snapshots.slice(0, state.currentIndex + 1)
    state.snapshots.push(clone(snapshot))
    state.snapshots = [...[],...state.snapshots]
    state.currentIndex++;
  },

  /** 
   * Setter for hasUnsavedChanges
   * 
   * @param {Boolean} hasChanges [default: true]
   */
  SET_UNSAVED_CHANGES: (state, hasChanges = true) => {
    state.hasUnsavedChanges = hasChanges
  }
}
