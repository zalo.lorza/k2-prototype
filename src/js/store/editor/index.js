import state from '@store/editor/state'
import getters from '@store/editor/getters'
import actions from '@store/editor/actions'
import mutations from '@store/editor/mutations'

/**
 * Here, we simply aggregate the definitions laid
 * out in each of the state, getter, action and
 * mutation files within this Vuex module.
 * 
 * State is a function to be able to reset it.
 */
export default () => {
  return {
    namespaced: true,
    state: state(),
    getters,
    actions,
    mutations
  }
}
