/**
 * @info
 *
 * *The "store/editor" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 *
 * *This file*
 * This file outlines "actions". Actions are functions that
 * include logic that, when ready, commits a mutation to the
 * application state. Whereas mutations must be synchronous,
 * actions return a promise and can include asynchronous
 * logical sequences.
 */
import router from '@router'
import api from '@api'
const { clone, capitalize } = util

export default {

  /** 
   * Adds new items (product lines)
   * Based on a difference counter, creates as many new items, 
   * these extended from the newItemDefaults object
   * - Adds text lines if specified
   * 
   * @param {Integer} counter - how many items to be added
   * 
   */

  createNewItems: ({state, commit, dispatch, getters}, counter = 1) => {
    let n = 0;
    while (n < counter) {
      const item = {
        isMaster: !state.items.length && n == 0 ? true : false,
        hasBeenEdited: false,
        elements: [],
        ...state.newItemDefaults
      }

      // Add item to store
      commit('ADD_ITEM', item)

      // New itemKey
      const itemKey = (state.items.length - 1)

      // Add New text elements to item
      if(item.textLines) {
        dispatch('batchCreateElements', { 
          itemKey, 
          quantity: item.textLines, 
          type:'text'}
        )
      }

      //Add slave items if needed
      if(!item.isMaster){
        state.availableTypes.forEach(type => {
          const masterElementsByType = getters.getMasterElementsByType(type)
          if(masterElementsByType.length){
            masterElementsByType.forEach(masterElement => {
              if(masterElement.isMaster) dispatch('setSlaveElement', { itemKey, masterElement })
            })
          }
        })
      }
      n++;
    }
    log.action(`Added ${counter} items`)
  },

  /** 
   * Modifies number of items (product lines)
   * It removes or adds items
   * 
   * @param {Integer} quantity - new number of items
   */

  updateQuantity: ({ dispatch, getters }, quantity) => {
    const quantityItems = getters['getQuantityItems']
    const difference = quantity - quantityItems
    if(difference > 0){
      dispatch('createNewItems', difference)
    } else if(difference < 0) {
      dispatch('removeLastItems', -difference)
    }
  },

  /** 
   * Removes a number of items (product lines)
   * Checks first if it's safe to delete, meaning that the affected items have not been yet edited by the user
   * 
   * @param {Object} difference - number of items to be removed
   * 
   * @listens router:safeAfterEach - needed to reroute if anoy of the removed items is the current route
   */

  removeLastItems: ({state, commit}, difference) => {
    if(difference < 1) return
    var isSafeToDelete = true
    const items = clone(state.items)
    const deletable = items.slice(items.length - difference)
    deletable.forEach(item => {
      if(item.hasBeenEdited) isSafeToDelete = false
    })
    if (isSafeToDelete || confirm(`Are you sure you want to remove ${difference} items?`)) {
      const newQuantity = items.length - difference
      const next = () => {
        commit('TRUNCATE_ITEMS', items.length - difference)
        log.action(`Removed ${difference} items`)
      }
      if(router.currentRoute.params && router.currentRoute.params.item && parseInt(router.currentRoute.params.item) > newQuantity){
        api.once('router:safeAfterEach', next)
        router.push({name: router.currentRoute.name, params: {item: newQuantity}})
      } else {
        next()
      }
    }
  },

  /** 
   * Create new text element
   * 
   * @param {Integer} itemIndex - item index
   * 
   */

  newTextElement: ({commit, state, getters}, itemIndex) => {
    const item = state.items[itemIndex]
    const elements = getters['getElementsByItemAndType']({item: itemIndex, type: 'text'})
    if(item.maxTextLines === elements.length) {
      log.warn('Cannot add more text lines to this item')
      return
    }
    const lineNumber = elements.length + 1
    const width = parseInt(getters.canvasWidth * 0.86)
    const y = parseInt(getters.canvasHeight * 0.16 * ((lineNumber%5) ? lineNumber%5 : 5))
    const x = parseInt((getters.canvasWidth - width) / 2)
    commit('CREATE_ELEMENT', { 
      item: itemIndex,
      element: { 
        type: 'text',
        config: {
          x,
          y,
          width,
          arc: 0,
          placeholder: `Line ${lineNumber} (Max ${state.elementDefaults.text.maxCharacters} Characters)`,
        }
      }
    })
  },

  /** 
   * @wip Create new image element 
   * (keep naming: structure @see batchCreateElements )
   * 
   */
  newImageElement: ({commit, getters}, { itemIndex, src, name, width, height }) => {
    
    const elementWidth = getters.canvasWidth / 2
    const elementHeight = elementWidth/width*height
    const x = getters.canvasCenter[0] - (elementWidth/2)
    const y = getters.canvasCenter[1] - (elementHeight/2)

    const image = new Image();
    image.onload = () => {
      commit('CREATE_ELEMENT', { 
        item: itemIndex,
        element: { 
          type: 'image',
          data: {
            src,
            name,
            width,
            height
          },
          config: {
            x,
            y,
            image,
            width: elementWidth,
            height: elementHeight
          }
        }
      })
    }
    image.src = src;
  },

  /** 
   * Create multiple elements in batch
   * 
   * @param {Object} data
   * 
   */

  batchCreateElements: ({dispatch}, {itemKey, quantity, type}) => {
    log.store(`Batch creating ${quantity} ${type} new elements on item ${itemKey}`)
    for(var i=0; i < quantity; i++){
      dispatch(`new${capitalize(type)}Element`, itemKey);
    }
  },

  /** 
   * Create/Undo Master<>Slave relation
   * 
   * @param {Object} data
   * 
   *    @property {Boolean}  makeSlaves  [optional, default: true] If true, item will become master, and non-master elements will become slaves. If false, it will revert this
   *    @property {Object}   element      Element object
   *    @property {Integer}  itemKey     [optional, default: masterKey] Master item index
   */
  setMasterAndSlaves: ({state, dispatch, commit, getters}, { async = true, makeSlaves = true, itemKey = null, masterElement }) => {
    if(!itemKey) itemKey = getters.getMasterKey
    if(getters.getMasterKey !== itemKey) return
    log.store(`${(makeSlaves ? 'Set':'Unset')} master<>slave elements`)

    // Set/Unset master
    commit('SET_MASTER_ELEMENT', { isMaster: makeSlaves, itemKey, elementKey: masterElement.elementKey })
    
    // Set/Unset slaves
    const setSlaves = () => {
      state.items.forEach((item, key) => {
        if(key != itemKey){
          dispatch('setSlaveElement', { itemKey: key, masterElement: { ...masterElement, isMaster: makeSlaves } })
        }
      })
      commit('REFRESH_ITEMS')
      api.snapshot()
    }

    // Resolve 'Set Slaves' (async or sync)
    if(async){
      clearTimeout(window.setMasterAndSlavesTimout)
      window.setMasterAndSlavesTimout = setTimeout(setSlaves, 100)
    } else {
      setSlaves()
    }
  },

  /** set slave element */
  setSlaveElement: ({getters, commit, dispatch}, { itemKey, masterElement }) => {
      const { keyByType, isMaster } = masterElement
      const itemElementsByType = getters.getElementsByItemAndType({item: itemKey, type: masterElement.type})
      if(typeof itemElementsByType[keyByType] !== 'undefined'){
        commit('SET_SLAVE_ELEMENT', { 
          isSlave: isMaster, 
          itemKey, 
          elementKey: itemElementsByType[keyByType].elementKey 
        })
      } else {
        const quantity = (keyByType + 1) - itemElementsByType.length
        dispatch('batchCreateElements', { 
          itemKey, 
          quantity, 
          type: masterElement.type 
        })
        commit('SET_SLAVE_ELEMENT', { 
          isSlave: isMaster, 
          itemKey: itemKey, 
          elementKey: getters.getElementsByItemAndType({item: itemKey, type: masterElement.type})[keyByType].elementKey 
        })
      }
  },

  /** 
   * Delete element from item
   * If element is master, will unlink slaves first
   * 
   * @param {Object} data
   * 
   *    @property {Object}   element    Element object
   *    @property {Integer}  itemKey    Item index
   *    @property {Boolean}  promptUser [default:false] if true, user will be prompted witha confirmation message before proceding
   */
  deleteElement: ({commit, dispatch}, { itemKey, element, promptUser = false }) => {
    if(!promptUser || confirm(`Are you sure you want to remove this ${element.type} element? This is irreversible`)){
      if (element.isMaster) {
        dispatch('deleteElementFromAllItems', element)
      } else {
        commit('REMOVE_ELEMENT', {itemKey, elementKey: element.elementKey})
      }
    }
  },

  /** 
   * Remove line element from all items
   * 
   * @param {Object} element Element object
   * 
   */
  deleteElementFromAllItems: ({dispatch, commit, getters, state}, element) => {
    log.warn('deleteElementFromAllItems')
    const { type, keyByType } = element
    dispatch('setMasterAndSlaves', { async: false, makeSlaves: false, masterElement: element })
    let deleteElements = []
    state.items.forEach((item, key)=> {
      const slaveElement = getters.getElementsByItemAndType({item: key, type})[keyByType]
      if(item.isMaster || !slaveElement.hasBeenEdited) deleteElements.push({itemKey: key, elementKey: slaveElement.elementKey})
    })
    deleteElements.forEach(slaveElement => {
      commit('REMOVE_ELEMENT', slaveElement)
    })
  }
}
