/**
 * @info
 *
 * *The "store/editor" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 *
 * *This file*
 * This file outlines "state". This is the starting snapshot
 * of the modules underlying state. As the user interacts with
 * the components tied into this Vuex module, this state is modified
 * away from these default values.
 */

 
/** 
* @property {Boolean}     showPreview           [default: true]    Show elements preview on the canvas panel (editable canvas using Konva)
* @property {Integer}     wrapperWidth          [default: 457]     Original wrapper width in pixels
* @property {Integer}     wrapperHeight         [default: 598]    Original wrapper height in pixels
* @property {Array}       canvasRelativeCenter  [default: [50,50]] Canvas relative center against wrapper in % - [(min: 0, max:100), (min: 0, max:100)]
* @property {Integer}     canvasRelativeWidth   [default: 100]     Canvas relative width against wrapper in % - (min: 1, max:100)
* @property {Integer}     canvasRelativeHeight  [default: 100]     Canvas relative height against wrapper in % - (min: 1, max:100)
* @property {URL|Boolean} backgroundImage       [default: false]   Canvas background image
* @property {Array}       items                                    Array of product copies with its settings (same product variant ID, different elements per item)
* @property {Array}       availableTypes                           Array of available element types
* 
* @property {Object}      newItemDefaults                          Default object used to create and extend from new items
*   | @property {Integer} newItemDefaults.textLines         [default: 0]     Default number of initial lines on new items
*   | @property {Boolean} newItemDefaults.lockImages        [default: false] If locked, image elements cannot be added/removed from the item
*   | @property {Boolean} newItemDefaults.lockTextLines     [default: false] If locked, text elements cannot be added/removed from the item
*   | @property {Boolean} newItemDefaults.textMaxCharacters [default: 25]    Max characters text lines
* 
* @property {Object}      elementDefaults                   Default values used on items' elements
*   | @property {Object}  elementDefaults.text              Default values used on items' text elements
*   | @property {Object}  elementDefaults.image             Default values used on items' image elements
*/

export default () => {
  return {
    availableTypes: ['text', 'image'],
    showPreview: true,
    wrapperWidth: 457,
    wrapperHeight: 598,
    canvasRelativeCenter: [50, 50],
    canvasRelativeWidth: 100,
    canvasRelativeHeight: 100,
    backgroundImage: false,
    textToolbar: [
      'fontFamily', 
      'fontSize', 
      'fontStyle', 
      'align', 
      'position', 
      'arc'
    ],
    items: [],
    newItemDefaults: {
      textLines: 0,
      lockImages: false,
      lockTextLines: false,
      maxTextLines: false,
      maxImages: false
    },
    elementDefaults: {
      text: {
        maxCharacters: 30,
        fontStyle: 'normal',
        fontSize: 18,
        align: 'center',
        fontFamily: 'NB Akademie Std',
        fill: 'black',
        text: ''
      },
      image: {
      }
    }
  }
}
