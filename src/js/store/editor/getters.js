/**
 * @info
 *
 * *The "store/editor" directory*
 * This "store" directory declares each of the Vuex

 * *This file*
 * This file outlines "getters". Getters are methods
 * that perform modifications to the state value prior
 * to returning the value to the consuming component.
 */
import router from '@router'

export default {

  /** 
   * Returns all the elements by type (text, image,...) from a specific item (product line)
   * 
   * @param {Object} payload 
   * 
   *      @property {Integer} item - item index
   *      @property {string}  type - elements type (text, image,...)
   * 
   * @returns {Array} Array of elements
   */

  getElementsByItemAndType: (state, getters) => ({item, type}) => {
    var keyByType = 0
    return getters.getElementsByItem(item).reduce((elements, element) => {
      if(element.type === type) {
        elements.push({...{},...element,...{ keyByType }})
        keyByType++
      }
      return elements
    }, [])
  },

  /** 
   * Returns all the elements from a specific item (product line)
   * 
   * @param {Integer} item - item index
   * 
   * @returns {Array} Array of elements
   */

  getElementsByItem: (state, getters) => item => {
    var elementKey = 0
    const thisItem = state.items[item]
    return thisItem.elements.reduce((elements, element) => {
      if(!thisItem.isMaster && element.isSlave){
        elements.push({...{}, ...getters.getMasterElements[elementKey], ...{ isSlave: true, isMaster: false }})
      } else {
        elements.push({...{}, ...element, ...{ elementKey }}) 
      }
      elementKey++
      return elements
    }, [])
  },

  /** 
   * Retrieves master item key (used for slave data)
   * For now, this will be always the first item (key = 0), but this is ready
   * to implement a  future method to switch master between diferent 
   * items if needed
   * 
   * @returns {Integer} Item key
   */

  getMasterKey: state => {
    var key = 0
    state.items.every(item => {
      if(item.isMaster) {
        return false
      }
      key++
      return true
    })
    return key
  },

  /** 
   * Returns all the elements from master item
   * 
   * @returns {Array} Array of elements
   */

  getMasterElements: (state, getters) => {
    return getters.getElementsByItem(getters['getMasterKey'])
  },

  /** 
   * Returns all the elements from master item by type
   * 
   * @returns {Array} Array of elements
   */

  getMasterElementsByType: (state, getters) => type => {
    return getters.getElementsByItemAndType({item: getters['getMasterKey'], type })
  },

  /** 
   * Returns curent item (product line) based on current route
   * 
   * @returns {Object}
   */

  currentItem: state => {
    return state.items[router.currentRoute.params.item - 1]
  },

  /** 
   * Returns canvas with in pixels
   * 
   * @returns {Integer}
   */

  canvasWidth: state => {
    return parseInt(state.wrapperWidth*(state.canvasRelativeWidth/100))
  },

  /** 
   * Returns canvas height in pixels
   * 
   * @returns {Integer}
   */

  canvasHeight: state => {
    return parseInt(state.wrapperHeight*(state.canvasRelativeHeight/100))
  },

  /** 
   * Returns canvas center in pixels
   * 
   * @returns {Integer}
   */

  canvasCenter: (state, getters) => {
    return [getters.canvasWidth / 2, getters.canvasHeight / 2]
  },

  /** 
   * Returns wrapper percentage ratio with/height
   * 
   * @returns {Float}
   */

  wrapperProportion: state => {
    return 100/state.wrapperWidth*state.wrapperHeight
  },

  /** 
   * Returns canvas percentage ratio with/height
   * 
   * @returns {Float}
   */

  canvasProportion: state => {
    const width = state.wrapperWidth*(state.canvasRelativeWidth/100)
    const height = state.wrapperHeight*(state.canvasRelativeHeight/100)
    return 100/width*height
  },

  /** 
   * Returns total items quantity (product lines)
   * 
   * @returns {Integer}
   */

  getQuantityItems: state => {
    return state.items.length
  },

  /** 
   * Returns state data to be saved 
   * 
   * @returns {Object}
   * @wip remove unnecessary data, since this data will be
   *      the one saved to the server
   */

  dataToSave: state => {
    return {
      ...state
    }
  }
}
