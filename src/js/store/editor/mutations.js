/**
 * @info
 *
 * *The "store/editor" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind editor functionality.
 *
 * *This file*
 * This file outlines "mutations". Mutations are intended
 * to be as simple as possible. All modifications to the state
 * object should happen inside a mutation function. Each
 * of these functions must be synchronous. There should be nothing
 * that has a time delay, such as an AJAX request or a setTimeout.
 */

import resetState from '@store/editor/state'
import api from '@api'
const { newUID } = util


export default {

  /** 
   * Resets state to default values (use to unload data)
   */

  RESET: state => {
    Object.assign(state, resetState(state.location))
  },

  /** 
   * Sets state values
   * 
   * @param {Object} data
   * 
   *    @property {Array}          items          [optional, default:[]] - Preset of items (product lines) 
   *    @property {Number}         width          [optional, default:100] - Relative canvas width to the wrapper (from 0.1 to 100)
   *    @property {Number}         height         [optional, default:100] - Relative canvas height to the wrapper (from 0.1 to 100)
   *    @property {Array|Number}   center         [optional, default:[50,50]] - Relative canvas center to the wrapper Array[(from 0 to 100), (from 0 to 100)] or Number[from 0 to 100]
   *    @property {Number|Boolean} maxTextLines   [optional, default:false] - Number of maximum text lines allowed on elements
   *    @property {Number|Boolean} maxImages      [optional, default:false] - Number of maximum images allowed on elements
   *    @property {Number}         textLines      [optional, default:1] - Number of default text lines on new elements
   *    @property {Number}         textToolbar    [optional] - Array of tools: ['fontFamily', 'fontSize', 'fontStyle', 'align', 'position', 'arc']
   *    @property {Boolean}        lockImages     [optional, default:false] - User cannot add/remove images to the item
   *    @property {Boolean}        lockTextLines  [optional, default:false] - User cannot add/remove text lines to the item
   *    @property {Boolean}        showPreview    [optional, default:true] - shows elements preview on the canvas panel (editable canvas using Konva)
   *    @property {URL}            image          [optional] - Image URL: if passed, will be used as backgroundImage on the canvas editor 
   * 
   * */

  SET: (state, { items, width, height, center, maxTextLines, maxImages, textLines, textToolbar, lockImages, lockTextLines, showPreview, image }) => {
    if(width) state.canvasRelativeWidth = width
    if(height) state.canvasRelativeHeight = height
    if(center) {
      if(!Array.isArray(center)) center = [center, center]
      state.canvasRelativeCenter = center
    }
    if(maxTextLines) state.newItemDefaults.maxTextLines = maxTextLines
    if(maxImages) state.newItemDefaults.maxImages = maxImages
    if(textLines)    state.newItemDefaults.textLines = textLines
    if(textToolbar)  state.textToolbar = textToolbar
    if(typeof lockImages !== 'undefined') state.newItemDefaults.lockImages = lockImages
    if(typeof lockTextLines !== 'undefined') state.newItemDefaults.lockTextLines = lockTextLines
    if(typeof showPreview !== 'undefined') state.showPreview = showPreview
    if(items) state.items = [...[], ...items]
    if(image) state.backgroundImage = image
  },
  
  /** 
   * Adds new item (product lines)
   * 
   * @param {Object} item - item object
   * 
   */
  ADD_ITEM: (state, item) => {
    state.items.push(item)
    log.storeSuccess('New item added')
    log.obj(item)
  },

  /** 
   * Reduces and truncates number of items (product lines)
   * 
   * @param {Integer} limit - how many items to be kept
   * 
   */

  TRUNCATE_ITEMS: (state, limit) => {
    state.items = state.items.slice(0, limit)
  },

  /** 
   * Creates new elements (text lines, images,...) on a specific item (product line)
   * 
   * @param {Object} data
   * 
   *    @property {Integer} item      Item index
   *    @property {Object}  element   New element configuration object 
   *    @property {Boolean} refresh   [default: false] If true, it'll refresh state.items object
   */

  CREATE_ELEMENT: (state, { item, element, refresh = false }) => {
    const id = `k2-${newUID()}`

    var defaults = { 
      order: Date.now() 
    }

    if(typeof state.elementDefaults[element.type] === 'object') defaults = {
      ...defaults, 
      ...state.elementDefaults[element.type] 
    }
    
    const factoryConfig = {
      ...defaults,
      ...element.config
    }

    const newElement = {
      ...element,
      id,
      isSlave: false,
      config: {
        scaleX: 1,
        scaleY: 1,
        draggable: true,
        ...factoryConfig
      },
      factoryConfig
    }
    state.items[item].elements.push(newElement)
    if(refresh) state.items = [[], ...state.items]
    log.storeSuccess(`New ${newElement.type} element created on item ${item}`)
    log.obj(newElement)
    api.snapshot()
  },

  /** 
   * Updates element config value
   * 
   * @param {Object} data
   * 
   *    @property {String}    key         Config key to be updated
   *    @property {Multiple}  value       Updated value
   *    @property {Integer}   elementKey  Element index
   *    @property {Integer}   itemKey     Item index
   */
  UPDATE_ELEMENT_CONFIG: (state, {key, value, elementKey, itemKey }) => {
    const element = state.items[itemKey].elements[elementKey]
    element.hasBeenEdited = true
    element.config[key] = value
    log.store(`Updated element`)
  },

  /** 
   * Converts/Reverses element as master 
   * (only if parent item is master)
   * 
   * @param {Object} data
   * 
   *    @property {Integer}   elementKey  Element index
   *    @property {Integer}   itemKey     Parent Item index
   *    @property {Boolean}   isMaster    [default: false] If true, element will be converted as master
   */
  SET_MASTER_ELEMENT: (state, {itemKey, elementKey, isMaster = true}) => {
    log.store(`${isMaster ? 'Set' : 'Unset'} master element`)
    state.items[itemKey].elements[elementKey].isMaster = isMaster
  },

  /** 
   * Converts/Reverses item as slave
   * 
   * @param {Object} data
   * 
   *    @property {Integer}   elementKey  Element index
   *    @property {Integer}   itemKey     Item index
   *    @property {Boolean}   isSlave     [default: false] If true, element will be converted as slave of master
   */
  SET_SLAVE_ELEMENT: (state, {itemKey, elementKey, isSlave}) => {
    log.store(`${isSlave ? 'Set' : 'Unset'} slave element`)
    state.items[itemKey].elements[elementKey].isSlave = isSlave
  },

  /* 
  * refreshes items object for reactivity purposes
  */
  REFRESH_ITEMS: state => {
    state.items = [...[], ...state.items]
  },

  /** 
   * Remove element from item
   * 
   * @param {Object} data
   * 
   *    @property {Integer}   elementKey  Element index
   *    @property {Integer}   itemKey     Item index
   */
  REMOVE_ELEMENT: (state, { itemKey, elementKey }) => {
    const element = state.items[itemKey].elements[elementKey]
    if(!element) return
    state.items[itemKey].elements.splice(elementKey, 1)
    log.storeDanger(`Removed ${element.type} element`)
    log.obj(element)
  }
}
