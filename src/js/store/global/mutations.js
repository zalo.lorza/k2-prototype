/**
 * @info
 *
 * *The "store/global" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 * Vuex store module that sits behind global functionality.
 *
 * *This file*
 * This file outlines "mutations". Mutations are intended
 * to be as simple as possible. All modifications to the state
 * object should happen inside a mutation function. Each
 * of these functions must be synchronous. There should be nothing
 * that has a time delay, such as an AJAX request or a setTimeout.
 */

export default {
  /** 
   * Setter method to stablish that the customizer global app has been injected and initialized,
   * and therefore it's ready to be used
   */
  INIT: state => {
    state.hasInitialized = true
  },

  /** 
   * Open the customizer app
   */
  OPEN: state =>  {
    state.isOpen = true
  },

  /** 
   * Close the customizer app
   */
  CLOSE: state =>  {
    state.isOpen = false
    log.redMilestone('Editor has closed')
  },

  /** 
   * Set product data
   */
  SET_PRODUCT: (state, product) => {
    state.product = {...{}, ...product}
  }
}
