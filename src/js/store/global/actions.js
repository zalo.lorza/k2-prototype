/**
 * @info
 *
 * *The "store/global" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 *
 * *This file*
 * This file outlines "actions". Actions are functions that
 * include logic that, when ready, commits a mutation to the
 * application state. Whereas mutations must be synchronous,
 * actions return a promise and can include asynchronous
 * logical sequences.
 */

export default {
}
