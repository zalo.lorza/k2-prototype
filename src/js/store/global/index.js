import state from '@store/global/state'
import getters from '@store/global/getters'
import actions from '@store/global/actions'
import mutations from '@store/global/mutations'

/**
 * Here, we simply aggregate the definitions laid
 * out in each of the state, getter, action and
 * mutation files within this Vuex module.
 */
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
