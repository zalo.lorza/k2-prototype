/**
 * @info
 *
 * *The "store/global" directory*
 * This "store" directory declares each of the Vuex
 * modules that all together power the site. Here, we have the
 *
 * *This file*
 * This file outlines "state". This is the starting snapshot
 * of the modules underlying state. As the user interacts with
 * the components tied into this Vuex module, this state is modified
 * away from these default values.
 */

/** 
* @property {Boolean}   hasInitialized   [default: false]  Global app has been injected and mounted, and it's ready to be used
* @property {Boolean}   isOpen           [default: false]  App is open 
* @property {Integer}   maxQuantity      [default: 30]     Maximum number of allowed items (product lines) 
* @property {Object}    Product          [default: null]   Incoming product object (used for pricing)
*/

export default {
  hasInitialized: false,
  isOpen: false,
  maxQuantity: 30,
  product: null
}
