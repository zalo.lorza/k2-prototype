/**
 * @info
 *
 * *The "store/global" directory*
 * This "store" directory declares each of the Vuex

 * *This file*
 * This file outlines "getters". Getters are methods
 * that perform modifications to the state value prior
 * to returning the value to the consuming component.
 */

 const { formatPrice } = util

export default {
  /** 
   * Returns product single price formatted
   * @wip logic for 'Bulk order discounts' will need to be applied here
   * 
   * @returns {String}
   */
  getSinglePrice: (state) => {
    return formatPrice(state.product.price)
  },

  /** 
   * Returns total price formatted
   * @wip logic for 'Bulk order discounts' will need to be applied here
   * 
   * @returns {String}
   */
  getTotalPrice: (state, getters, rootState, rootGetters) => {
    return formatPrice(state.product.price * rootGetters["editor/getQuantityItems"])
  }
}
