import Vue from 'vue'
import Vuex from 'vuex'
import editor from '@store/editor'
import history from '@store/history'
import global from '@store/global'
Vue.use(Vuex)

/**
 * Here, we take all of the independent
 * Vuex modules laid out in the store
 * subdirectories and create a single Vuex
 * store that includes all of them.
 * 
 * @alias $store on vue components
 */
export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {},
  modules: {
    editor: editor(),
    history: history(),
    global
  }
})
