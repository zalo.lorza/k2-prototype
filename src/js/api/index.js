import Vue from 'vue'
import * as events from './methods/events'
import * as init from './methods/init'
import * as nav from './methods/nav'
import * as history from './methods/history'
import * as cart from './methods/cart'
import * as store from './methods/store'
import * as getters from './methods/getters'

/** 
 * @protected API object
 * 
 * Object of available methods to simplify and centralize app arquitecture
 * and to expose public methods to the window object
 * It also serves as bus events
 * 
 * @public    @alias K2Customizer on browser Window object
 * @protected @alias $api on Vue components
 * 
 * @docs we use visibility block tags in the comments:
 *    | @private    method not exposed on the api object, is an internal helper
 *    | @protected  method exposed on the api object, but only available inside of the app
 *    | @public     method exposed on the alias window object `K2Customizer`, publicly available everywhere
 * 
 * */

let api = {
  ...events,
  ...init,
  ...nav,
  ...history,
  ...cart,
  ...store,
  ...getters
}

/**
 * @extend Vue
 * 
 * Available as this.$api on Vue components
 */
Vue.prototype.$api = api


/**
 * @extend Window
 * 
 * Exposes api to the window browser
 * This is a subset version of the api object
 * 
 * Inludes methods
 *   - events: on, once, off 
 *   - history: save
 *   - nav: prev, next, go
 *   - cart: addToCart
 *   - getters: hasInitialized, isOpen
 * 
 * Adds aliases
 *   - init (initApp)
 *   - new (newCustomizer)
 *   - open (openSavedCustomizer)
 * 
 */
window.K2Customizer = {
  ...getters,
  ...nav,
  addToCart: api.addToCart,
  save: api.save,
  on: api.on,
  once: api.once,
  off: api.off,
  init: api.initApp,
  new: api.newCustomizer,
  open: api.openSavedCustomizer
} 


/*
* Export full api to the app
*/
export default api