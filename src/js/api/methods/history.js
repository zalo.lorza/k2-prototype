import store from '@store'


/** 
 * @protected api.snapshot
 * 
 * Attempts to create a snapshot from the current data
 * This action is debounced to avoid performance issues
 * If current data is equal as previous snapshot, new snapshot won't be created
 * 
 * */

export const snapshot = () => {
  store.dispatch('history/newSnapshot')
}

/** 
 * @public api.save
 * 
 * Save current data (product items and their custom elements)
 * Currently it downloads a JSON file locally, eventually this will save into a remote DB
 * Suggestion: compress the data before sending it to the server
 * */

export const save = () => {
  log.api('Save')
  store.dispatch('history/save')
}