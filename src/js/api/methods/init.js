import Vue from 'vue'
import store from '@store'
import router from '@router'
import App from '@views/app/app.vue'
import api from '@api'
const { newUID } = util

/** 
 * @protected     api.initApp
 * @public @alias K2Customizer.init
 * 
 * Injects and mounts app on the DOM
 * This method can be called only once, unless we add a kill/destroy method
 * When app has already mounted, it still fires callback passed as argument
 * 
 * @param {Function} callback - will fire after app is initialized and mounted
 * 
 * */

export const initApp = (callback = () => {}) => {

  // Returns if app has already initialized but still fires callback
  if(api.hasInitialized()) {
    log.warn('K2 Customizer already initialized')
    callback()
    return
  }
  store.commit('global/INIT')
  log.api('init')
  log.msg('Initializing app')

  // Inject DOM element
  const id = `k2-customizer-${newUID()}`
  const el = document.createElement('div')
  el.setAttribute("id", id)
  document.body.appendChild(el)

  // Mount app
  new Vue({
    el,
    store,
    router,
    render: h => h(App),
    mounted(){
      log.milestone('App mounted')
      callback()
    }
  })
}

/** 
 * @private openApp
 * 
 * Opens the app making sure this is first initialized (mounted on the page DOM)
 * 
 * @param {Object} data - data to be loaded into the editor
 * @param {function} beforeOpen [optional] - 
 * 
 * */

const openApp = (data, beforeOpen = () => {}) => {
  const open = () => {
    api.resetData()
    log.api('open')
    store.commit('global/OPEN')
    const { editor = 'full' } = data
    router.push('/loading')
    log.msg(`Opening ${editor} editor`)
    store.commit('global/SET_PRODUCT', data.product)
    store.commit('editor/SET', data)

    //Prepare from the caller method
    beforeOpen(data)
    
    //Open customizer editor
    var route = `/editor/${editor}/1`
    setTimeout(() => {   // #REMOVE (fakes a loading time)
      router.push(route)
      log.milestone(`${editor} editor opened`)
      log.obj(data)
    }, 400)
  }

  // Make sure app has initialized before opening the customizer
  if(!api.hasInitialized()) {
    api.initApp(open)
  } else {
    open()
  }
}

/** 
 * @protected     api.newCustomizer
 * @public @alias K2Customizer.new
 * 
 * Create/Open new Customizer
 * 
 * @param {Object} data - data to be loaded into the editor
 * 
 * */

export const newCustomizer = data => {
  log.clear('New customizer')
  openApp(data, () => {
    store.dispatch('editor/updateQuantity', (data.quantity ?? 1))
  })
}

/** 
 * @protected     api.openSavedCustomizer
 * @public @alias K2Customizer.open
 * 
 * @wip Load/Open previously saved customizer
 * 
 * @param {Object} data - data to be loaded into the editor
 * 
 * */
export const openSavedCustomizer = data => {
  log.clear('Open saved customizer')
  openApp(data, () => {
  
  })  
}

/** 
 * @public api.close
 * 
 * Close customizer
 * Promts user first if unsaved changes
 * 
 * */
export const close = () => {
  log.api('close')
  if(store.state.history.hasUnsavedChanges){
    if(confirm('You have unsaved changes, are you sure you want to close your customizer?')) store.commit('global/CLOSE')
  } else {
    store.commit('global/CLOSE')
  }
}