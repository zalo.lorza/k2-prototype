
import router from '@router'
import store from '@store'
import api from '@api'

/** 
 * @private
 * 
 * This allows the user to fast navigate by allowing the next and prev 
 * action to be triggered multiple times even if previous navigation has 
 * not resolved yet 
 * 
 * @listens route:afterEach
 * 
*/
var cachedCurrent = null
const setCachedCurrent = () => {
  cachedCurrent = parseInt(router.currentRoute.params.item)
}
setTimeout(() => {
  api.on('route:afterEach', setCachedCurrent)
})

/** 
 * @public api.go
 * 
 * Go to specific index route
 * Improvement: make sure item exists
 * 
 * @param {integer} item - index route
 * @example api.go(2)
*/
export const go = (item = null) => {
  if(!item) item = cachedCurrent
  else cachedCurrent = item
  if(parseInt(router.currentRoute.params.item) !== item){
    router.push({name: router.currentRoute.name, params: { item } })
  }
}

/** 
 * @public api.prev
 * 
 * Go to previous product item
 * It loops, meaning that if router is in the first item, it'll jump to the last one
 * 
 * */

export const prev = () => {
  log.api('prev')
  if(!cachedCurrent) setCachedCurrent()
  if(cachedCurrent === 1){
    cachedCurrent = store.getters['editor/getQuantityItems']
  } else {
    cachedCurrent--
  }
  go(cachedCurrent)
}

/** 
 * @public api.next
 * 
 * Go to next product item
 * It loops, meaning that if router is in the last item, it'll go back to the first one
 * 
 * */

export const next = () => {
  if(!cachedCurrent) setCachedCurrent()
  if(cachedCurrent ===  store.getters['editor/getQuantityItems']){
    cachedCurrent = 1
  } else {
    cachedCurrent++
  }
  log.api(`next: ${cachedCurrent}`)
  go(cachedCurrent)
}