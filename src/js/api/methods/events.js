import Vue from 'vue'

/**
 * @private Events bus
 * 
 * Bus object for events across the app
 * 
 */
const bus = new Vue({})

/** 
 * @public api.on
 * 
 * Subscribes and listens indefinitely to bus events
 * 
 * @param {string} eventName - event name
 * @param {Function} callback - event callback
 */

export const on = (eventName, callback = () =>{}) => {
  return bus.$on(eventName, callback)
}


/** 
 * @public api.once
 * 
 * Listens only once a specific bus event
 * 
 * @param {string} eventName - event name
 * @param {Function} callback - event callback
 */

export const once = (eventName, callback = () =>{}) => {
  return bus.$once(eventName, callback)
}

/** 
 * @public api.off
 * 
 * Unsubscribes event listener
 * 
 * @param {string} eventName - event name
 * @param {Function} callback - event callback
 */

export const off = (eventName, callback = () =>{}) => {
  return bus.$off(eventName, callback)
}

/** 
 * @protected api.emit
 * 
 * Emits bus event
 * 
 * @param {string} eventName - event name
 * @param {Function} data [optional] - event data
 */

export const emit = (eventName, data = null) => {
  return bus.$emit(eventName, data)
}