
import store from '@store'
import api from '@api'

/** 
 * @public api.addToCart
 * 
 * Add to Cart action
 * @wip This method is faked with a placeholder at the moment
 * 
 * @fires loading
 */

export const addToCart = () => {
  api.emit('loading', `Adding ${store.getters['editor/getQuantityItems']} items to cart`)
  log.api('Add To Cart')
  setTimeout(() => {
    alert('This is a placeholder for the "Add To Cart" action')
    log.milestone(`Added ${store.getters['editor/getQuantityItems']} items to the cart`)
    api.emit('loading', false)
  }, 100)
}