import store from '@store'

/** 
 * @public Getter api.hasInitialized()
 * 
 * Checks if app has initalized and mounted on the DOM, and is ready to open
 * 
 * @returns {Boolean} If true: app has initialized
 */

export const hasInitialized = () => {
  return store.state.global.hasInitialized
}


/** 
 * @public Getter api.isOpen()
 * 
 * Checks if app is open and the modal is loaded and visible to the user
 * 
 * @returns {Boolean} If true: app is opn
 */

export const isOpen = () => {
  return store.state.global.isOpen
}