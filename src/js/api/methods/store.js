import store from '@store'


/**
 * @protected api.resetData
 * 
 * Resets Vuex store data
 * Useful to load a new editor
 */

export const resetData = () => {
  log.api('reset')
  store.commit('editor/RESET')
  store.commit('history/RESET')
  store.commit('editor/REFRESH_ITEMS')
}