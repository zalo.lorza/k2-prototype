import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import api from '@api'

Vue.use(VueRouter)

/**
 * Vue Router object
 * 
 * @param {string} mode - 'abstract' means the router won't manipualte browser's history, nor hash
 * @param {Object} routes - routes assigned to its views and metadata
 */
const router = new VueRouter({
  mode: 'abstract',
  routes
})

/**
 * Timeout var used to cancel routing timed out events
 */
var routingTimeout = null

/**
 * Before each route
 *
 * @param {function} callback - Callback function called before each route
 * 
 *  Callback function gets 3 params:
 *  |
 *  | @param {Route Object} to - next route
 *  | @param {Route Object} from  - previous/current route
 *  | @param {Function} next - routing resolve function, @important -> next() needs to be called to resolve routing 
 * 
 * @fires router:beforeEach - This event always fires, at any attempt of routing
 * @fires router:safeBeforeEach - This event only fires if routing has resolved (previous routing can be cacelled due to other routing events)
 * 
 */
router.beforeEach((to, from, next) => {
  clearTimeout(routingTimeout)
  api.emit('router:beforeEach')

  /* 
  * Resolution function
  */
  const go = () => {
    clearTimeout(routingTimeout)
    api.emit('router:safeBeforeEach')
    return next()
  }

  /* 
   * Not debounced routing resolution
   */
  if(to.name === 'loading' || from.name === 'loading') return go()

  /*
  * Small timeout to debounce routing and make sure routing only resolves 
  * after many consecutive user actions (for example clicking on next/prev 
  * button many times in a row)
  */
  routingTimeout = setTimeout(() => { 
    go()
  }, 100)
})

/**
 * After each route
 *
 * @param {function} callback - Callback function called after each route
 * 
 *  Callback function gets 3 params:
 *  |
 *  | @param {Route Object} to - current/new route
 *  | @param {Route Object} from  - previous route
 * 
 * @fires router:afterEach - it always fires after routing has resolved
 * @fires router:safeAfterEach - it only fires if no other routing has occurred in the following 100ms after resolving
 *
 */
router.afterEach((to) => {
  clearTimeout(routingTimeout)
  log.router(`${to.path}`)
  api.emit('router:afterEach')
  routingTimeout = setTimeout(() => {
    api.emit('router:safeAfterEach')
  }, 100)
})

export default router