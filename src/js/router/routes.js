import loading from '@views/loading/loading.vue'
const editor = () => import('@views/editor/editor.vue')

const footer = {
  editor: () => import('@views/editor/editor-footer/editor-footer.vue')
}

const canvas = {
  full: () => import('@views/editor/editor-canvas/editor-canvas-full.vue'),
  plate: () => import('@views/editor/editor-canvas/editor-canvas-plate.vue')
}

const settings = {
  single: () => import('@views/editor/editor-settings/editor-settings-single.vue'),
  multiple: () => import('@views/editor/editor-settings/editor-settings-multiple.vue')
}

export default [
  { path: '/loading', name: 'loading', component: loading, meta: { modal: false }  },
  { 
    path: '/editor', name: 'editor', components: { default: editor, footer: footer.editor },
    children: [
      { path: 'full/:item', 
        name: 'editor-full', 
        components: { canvas: canvas.full, settings: settings.single }, 
        meta: { 
          modalHasNextPrev: true, 
          title: 'Plate {{this.$route.params.item}} of {{this.$store.getters["editor/getQuantityItems"]}}' 
        } 
      },
      { path: 'plate/:item', 
        name: 'editor-plate', 
        components: {canvas: canvas.plate, settings: settings.multiple} 
      }
    ]
  }
]