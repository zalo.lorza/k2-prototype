/** 
 * Checks if number is an integer
 * 
 * @param {Number|String} n
 * @returns {Boolean}
 * 
*/
export const isInteger = (n) => {
  return Number(n) === n && n % 1 === 0;
}

/** 
 * Checks if number is a float
 * 
 * @param {Number|String} n
 * @returns {Boolean}
 * 
*/
export const isFloat = (n) => {
  return Number(n) === n && n % 1 !== 0;
}