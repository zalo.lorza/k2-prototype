/** 
 * Save and deep clone of an object
 * 
 * @param {Object} obj - object to be cloned
 * @returns {Object} - independent clone of the original object
 * 
*/
export const clone = (obj) => {
  return JSON.parse(JSON.stringify(obj))
}