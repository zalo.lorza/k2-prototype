import { prettyLog } from 'pretty-browser-log'
import Vue from 'vue'

/** 
 * Different log styles
 */
prettyLog.addManyLogStyles([
    {name:'danger',badge:'Danger',badgeStyle:'background-color:red;',messageStyle:'color:red'},
    {name:'delete',badge:'Deletion',badgeStyle:'background-color:red;',messageStyle:'color:red'},
    {name:'storeDanger',badge:'Vuex',badgeStyle:'background-color:red;',messageStyle:'color:red'},
    {name:'warn',badge:'Warning',badgeStyle:'background-color:orange;',messageStyle:'color:orange'},
    {name:'action',badge:'Action',badgeStyle:'background-color:#6b5b95',messageStyle:'color:#6b5b95'},
    {name:'success',badge:'Success',badgeStyle:'background-color:#4CD964',messageStyle:'color:#4CD964;'},
    {name:'storeSuccess',badge:'Vuex',badgeStyle:'background-color:#4CD964',messageStyle:'color:#4CD964;'},
    {name:'milestone',badge:'',badgeStyle:'border: 0',messageStyle:'color:#4CD964; border: 1px solid;border-radius:0.4rem;padding:2px 6px;'},
    {name:'redMilestone',badge:'',badgeStyle:'border: 0',messageStyle:'color:red; border: 1px solid;border-radius:0.4rem;padding:2px 6px;'},
    {name:'clear',badge:'',badgeStyle:'border: 0',messageStyle:'color:orange; border: 1px solid;border-radius:0.4rem;padding:10px 20px; font-size: 1.35em;'},
    {name:'router',badge:'Router',badgeStyle:'background-color:#6b5b95',messageStyle:'color:#6b5b95; border: 1px solid;border-radius:0.4rem;padding:2px 6px'},
    {name:'api',badge:'API',badgeStyle:'background-color:#6b5b95',messageStyle:'color:#6b5b95'},
    {name:'store',badge:'Vuex',badgeStyle:'background-color:#6b5b95',messageStyle:'color:#6b5b95'},
    {name:'msg',badge:'',messageStyle:''}
])

/** 
 * Logs an object
 * 
 * @param {object} obj
 * 
*/
const logObj = (obj) => {
  console.log({...{},...obj})
}

/** 
 * Adds a new blank line and a big title log
 * 
 * @param {string} message
 * 
*/
const clear = (message) => {
  console.log('')
  prettyLog.clear(`${message.toUpperCase()}`)
}

/** 
 * All the methods
 * 
*/
const log = {
  danger: prettyLog.danger,
  delete: prettyLog.delete,
  store: prettyLog.store,
  storeSuccess: prettyLog.storeSuccess,
  storeDanger: prettyLog.storeDanger,
  action: prettyLog.action,
  success: prettyLog.success,
  milestone: prettyLog.milestone,
  msg: prettyLog.msg,
  router: prettyLog.router,
  api: prettyLog.api,
  warn: prettyLog.warn,
  redMilestone: prettyLog.redMilestone,
  clear: clear,
  obj: logObj
}

/** 
 * @extends Vue
 * @alias $log in Vue components
 * 
*/
Vue.prototype.$log = log

export default log