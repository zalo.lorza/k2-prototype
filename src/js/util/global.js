/** 
 * Returns a new unique ID
 * 
 * @returns {integer} uid
 * 
*/
window.k2CustomizerUniqueID = Date.now()
export const newUID = () => {
  window.k2CustomizerUniqueID++
  return window.k2CustomizerUniqueID
}