/** 
 * Formats date
 * 
 * @param   {Date}   date   [optional] - Date() object, will default to now if null
 * @param   {String} format [optional] - date format
 * 
 * @returns {String} formated date
 * 
*/
export const formatDate = (date = false, format = 'mm/dd/yy') => {
  if(!date) date = new Date()
  const map = {
      mm: date.getMonth() + 1,
      dd: date.getDate(),
      yy: date.getFullYear().toString().slice(-2),
      yyyy: date.getFullYear()
  }

  return format.replace(/mm|dd|yy|yyy/gi, matched => map[matched])
}