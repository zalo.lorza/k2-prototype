import Vue from 'vue'
import * as date from './date'
import log from './log'
import * as global from './global'
import * as number from './number'
import * as object from './object'
import * as product from './product'
import * as text from './text'

const util = {
  log,
  ...date,
  ...global,
  ...number,
  ...object,
  ...product,
  ...text
}

Vue.prototype.$util = util

export default util