import { isInteger } from '@util/number'

/**
 * Formats price: 100,00 -> $100.00
 * Formats a given integer to a standard fraction.
 * It also converts price to cents in case it's not
 *
 * @param {Integer, Float} num - A price, with cents
 * @param {Integer} fraction [optional] - decimals, default: 2
 * @returns {String} - formatted price
 */
export const formatPrice = (num, fraction = 2) => {
  if(!isInteger(num)) num = Number(num) * 100 // normalize to cents
  return '$' + (Number(num) / 100).toLocaleString('en-EN', {
    minimumFractionDigits: fraction
  })
}