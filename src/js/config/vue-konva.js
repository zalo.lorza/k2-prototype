
import Vue from 'vue'
import VueKonva from 'vue-konva'

/**
 * Use Konva components as <k2-stage />, <k2-circle />,...
 * 
 * @docs https://konvajs.org/docs/vue/index.html
 */
Vue.use(VueKonva, { prefix: 'k2'});