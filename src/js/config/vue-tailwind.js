import Vue from 'vue'
import VueTailwind from 'vue-tailwind'

import TInput from 'vue-tailwind/dist/t-input';
import TCheckbox from 'vue-tailwind/dist/t-checkbox';
import TButton from 'vue-tailwind/dist/t-button';
import TDropdown from 'vue-tailwind/dist/t-dropdown';

/**
 * Vue Tailwind custom components configuration
 * 
 * @docs https://www.vue-tailwind.com/
 */
const settings = {
  TInput: {
    component: TInput,
    props: {
      fixedClasses: 'block w-full px-3 py-2 transition duration-100 ease-in-out border rounded-none shadow-none focus:ring-2 focus:ring-blue-500 focus:outline-none focus:ring-opacity-50 disabled:opacity-20 disabled:cursor-not-allowed',
      classes: 'text-black placeholder-gray-400 bg-white border-black focus:border-blue-500 ',
      variants: {}
    }
  },
  TCheckbox: {
    component: TCheckbox,
    props: {
      fixedClasses: 'w-5 h-5 transition duration-100 ease-in-out rounded-none shadow-none cursor-pointer focus:border-blue-500 focus:ring-2 focus:ring-blue-500 focus:outline-none focus:ring-opacity-50 focus:ring-offset-0 disabled:opacity-20 disabled:cursor-not-allowed',
      classes: 'text-blue-500 border-black',
      variants: {
        error: 'text-red-500 border-red-300',
        success: 'text-green-500 border-green-300'
      }
    }
  },
  TButton: {
    component: TButton,
    props: {
      fixedClasses: 'disabled:cursor-not-allowed transition duration-300 outline-none appearence-none focus:outline-none',
      classes: 'text-white bg-blue-500 rounded-full px-6 py-2 text-button border border-transparent shadow-sm rounded hover:bg-blue-600 min-h-10',
      variants: {
        link: 'text-blue-500 hover:underline disabled:opacity-50 disabled:no-underline hover:text-blue-600 border-0',
        small: 'text-white bg-blue-500 disabled:bg-gray-700 disabled:opacity-50  rounded-full px-3 py-1 text-button--small border border-transparent shadow-sm rounded hover:bg-blue-600 min-h-7'
      }
    }
  },
  TTool: {
    component: TButton,
    props: {
      fixedClasses: 'ttool text-xs w-5 h-5 disabled:cursor-not-allowed transition duration-300 outline-none appearence-none focus:outline-none rounded inline-flex items-center justify-center',
      classes: 'text-gray-900',
      variants: {
        selected: 'text-white bg-gray-900'
      }
    }
  },
  TDropdown: {
    component: TDropdown,
    props: {
      fixedClasses: {
        button: 'tdropdown-button relative whitespace-nowrap max-w-15 overflow-hidden px-0 flex items-center transition duration-100 ease-in-out focus:outline-none disabled:opacity-50 disabled:cursor-not-allowed',
        wrapper: 'inline-flex flex-col',
        dropdownWrapper: 'relative z-10',
        dropdown: 'origin-top-left absolute left-0 w-auto border border-black min-w-full shadow mt-1',
        enterActiveClass: 'transition ease-out duration-100 transform opacity-0 scale-95',
        enterToClass: 'transform opacity-100 scale-100',
        leaveClass: 'transition ease-in transform opacity-100 scale-100',
        leaveActiveClass: '',
        leaveToClass: 'transform opacity-0 scale-95 duration-75'
      },
      classes: {
        button: '',
        dropdown: 'bg-white'
      },
      variants: {}
    }
  }
}

Vue.use(VueTailwind, settings)

