import mixin from '@components/edit-element-text/mixin'
import ToolFontFamily from '@components/text-tools/font-family.vue'
import ToolFontSize from '@components/text-tools/font-size.vue'
import ToolFontStyle from '@components/text-tools/font-style.vue'
import ToolAlign from '@components/text-tools/align.vue'
import ToolPosition from '@components/text-tools/position.vue'
import ToolArc from '@components/text-tools/arc.vue'

export default {
  mixins: [ mixin ],
  components: {
    ToolFontFamily,
    ToolFontSize,
    ToolFontStyle,
    ToolAlign,
    ToolPosition,
    ToolArc
  }
}