import PanelImages from '@components/edit-panel-images/edit-panel-images.vue'
import PanelTexts from '@components/edit-panel-texts/edit-panel-texts.vue'

export default {
  components: { PanelImages, PanelTexts },
  props: ['index'],
  computed: {
    showImages(){
      return !this.$store.state.editor.items[this.index].lockImages
    }
  }
}