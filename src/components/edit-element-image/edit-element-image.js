export default {
  props: ['item', 'element', 'itemKey', 'elements'],
  computed: {
    config(){
      return this.element.config
    },
    elementKey(){
      return this.element.elementKey
    }
  },
  methods: {
    updateElementConfig({key, value}){
      this.$store.commit('editor/UPDATE_ELEMENT_CONFIG', {
        key,
        value,
        elementKey: this.elementKey,
        itemKey: this.itemKey,
      })
    },
    deleteElement(){
      this.$store.dispatch('editor/deleteElement', { promptUser: false, itemKey: this.itemKey, element: this.element })
    }
  }
}