import mixin from '@components/edit-element-text/mixin'
import TextToolbar from '@components/text-toolbar/text-toolbar.vue'
export default {
  mixins: [mixin],
  components: { TextToolbar },
  data(){
    return {
      text: ''
    }
  },
  methods: {
    onRefreshSlaves(master){
      if(master.keyByType === this.element.keyByType) this.updateInput()
    },
    updateInput(){
      this.text = this.element.config.text
    },
    updateText(text){
      if(this.element.isSlave || text === this.config.text) return
      if (text.length > this.maxCharacters) {
        this.$nextTick(() => {
          this.text = this.text.substring(0, this.maxCharacters)
        })
        return
      }
      this.updateElementConfig({key: 'text', value: text})
    }
  },
  mounted(){
    this.updateInput()
    this.$api.on('refreshSlaves', this.onRefreshSlaves)
  },
  beforeDestroy(){
    this.$api.off('refreshSlaves', this.onRefreshSlaves)
  }
}