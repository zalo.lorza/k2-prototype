export default {
  props: ['item', 'element', 'itemKey', 'elements'],
  computed: {
    config(){
      return this.element.config
    },
    elementKey(){
      return this.element.elementKey
    },
    canDelete(){
      return this.elements.slice(this.element.keyByType + 1).every(element => {
        if (element.isMaster || element.isSlave) return false
        return true
      })
    },
    maxCharacters(){
      return this.element.config.maxCharacters
    },
    isSlaveOrMaster(){
      return (!this.item.isMaster && this.element.isSlave) || (this.item.isMaster && this.element.isMaster)
    }
  },
  methods: {
    toogleMaster(value){
      if(!this.item.isMaster || value === this.element.isMaster) return;
      this.$store.dispatch('editor/setMasterAndSlaves', { 
        makeSlaves: value, 
        itemKey: this.itemKey, 
        masterElement: this.element 
      })
    },
    updateElementConfig({key, value}){
      this.$store.commit('editor/UPDATE_ELEMENT_CONFIG', {
        key,
        value,
        elementKey: this.elementKey,
        itemKey: this.itemKey,
      })
      this.refreshSlaves()
    },
    refreshSlaves(){
      clearTimeout(this.debounceRefresh)
      if(this.element.isMaster){
        this.debounceRefresh = setTimeout(() => {
          this.$store.commit('editor/REFRESH_ITEMS')
          this.$api.emit('refreshSlaves', this.element)
        }, 300)
      }
    },
    deleteElement(){
      this.$store.dispatch('editor/deleteElement', { promptUser: (this.config.text !== ''), itemKey: this.itemKey, element: this.element })
    }
  }
}