import Vue from 'vue'
import Icon from 'ant-design-vue/lib/icon'
import 'ant-design-vue/lib/icon/style/css'
import CarbonIcon from './carbon-icon'
/**
 * Register <Icon /> component
 * 
 * @docs https://antdv.com/
 */
Vue.component('Icon', Icon)

/**
 * Register Carbon icons
 * 
 * @docs https://github.com/carbon-design-system/carbon/tree/master/packages/icons-vue
 * 
 */
Vue.component('CarbonIcon', CarbonIcon)