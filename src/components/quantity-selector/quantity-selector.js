import NumberInput from '@chenfengyuan/vue-number-input'

export default {
  components: { NumberInput },
  computed: {
    quantity(){
      return this.$store.getters['editor/getQuantityItems']
    },
    max(){
      return this.$store.state.global.maxQuantity
    }
  },
  methods: {
    updateQuantity(val){
      if(this.quantity == val) return
      this.$store.dispatch('editor/updateQuantity', val)
    }
  }
}