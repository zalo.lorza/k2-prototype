import EditElement from '@components/edit-element-text/edit-element-text.vue'

export default {
  props: ['index'],
  components: { EditElement },
  computed:{
    item(){
      return this.$store.state.editor.items[this.index]
    },
    elements(){
      return this.$store.getters['editor/getElementsByItemAndType']({item: this.index, type: 'image'})
    }
  },
  methods: {
    onInputImage(e){
      if (e.target.files && e.target.files[0]) {
        var reader = new FileReader();
        const { name } = e.target.files[0]
        
        reader.onload = e => {
          const image = new Image();
          var self = this
          image.onload = function() {
            self.$store.dispatch('editor/newImageElement', {
              itemIndex: self.index,
              src: image.currentSrc,
              name,
              width: this.width,
              height: this.height
            })
          }
          image.src =  e.target.result;
        }
        
        reader.readAsDataURL(e.target.files[0]);
      }

      return false;
    }
  }
}