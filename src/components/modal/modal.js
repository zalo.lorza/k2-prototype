import Loading from '@components/loading/loading.vue'

export default {
  props: ['show'],
  components: { Loading },
  data(){
    return {
      loadingProcesses: 0,
      loadingMessage: null
    }
  },
  computed: {
    isLoading(){
      return this.loadingProcesses
    },
    loadingLabel(){
      if(typeof this.loadingMessage == 'string') return this.loadingMessage
      return 'Loading'
    }
  },
  methods: {
    onLoading(loading){
      if(loading) {
        this.loadingMessage = loading
        this.loadingProcesses++
      }
      else this.loadingProcesses--
      if(this.loadingProcesses < 0) this.loadingProcesses = 0
    }
  },
  mounted(){
    this.$api.on('loading', this.onLoading)
    this.$api.on('router:beforeEach', () => {
      this.loadingProcesses = 1
    })
    this.$api.on('router:safeAfterEach', () => {
      this.onLoading(false)
    })
  },
  beforeDestroy(){
    this.$api.off('loading', this.onLoading)
  }
}