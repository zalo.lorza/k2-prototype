
export default {
  data() {
    return {
      zIndex: 0,
      isDragging: false,
      dragItemId: null,
      selectedNode: null,
      selectedElementId: null
    };
  },
  computed:{
    orderedEements(){
      return [ ...[], ...this.elements ].sort((a, b) => {
        if ( a.isSlave || a.config.order < b.config.order ){
          return -1;
        }
        if ( b.isSlave || a.config.order > b.config.order ){
          return 1;
        }
        return 0;
      })
    },
    selectedElement(){
      return this.getElementById(this.selectedElementId)
    },
    stageConfig(){
      return {
        width: this.$store.getters['editor/canvasWidth'],
        height: this.$store.getters['editor/canvasHeight']
      }
    },
    elements(){
      return this.$store.getters['editor/getElementsByItem'](this.itemKey)
    },
    itemKey(){
      return this.$route.params.item - 1
    }
  },
  methods: {
    getElementById(id){
      var index = null
      const element = this.elements.find((r, k) => {
        if (r.id === id) {
          index = k
          return true
        }
        return false
      })
      return {...element, ...{ index }}
    },
    updateSelectedElement(config = {}){
      this.updateElement(this.selectedElement.index, config)
    },
    updateElement(elementKey, config){
      Object.keys(config).forEach(key => {
        this.$store.commit('editor/UPDATE_ELEMENT_CONFIG', {
          key,
          value: config[key],
          elementKey,
          itemKey: this.itemKey,
        })
      })
      this.updateTransformer()
    },
    getElementDynamicConfig({ type, id, config, isSlave }){
      var dynamicConfig = { id, draggable: (config.draggable && !isSlave) }
      switch(type){
        case 'text':
          dynamicConfig.fill = isSlave ? 'rgba(0,0,0,0.2)' : (this.isDragging === id ? 'rgba(0,0,0,0.5)' : 'black')
          break
      }
      return { ...config, ...dynamicConfig }
    },
    handleDragStart(e) {
      this.isDragging = e.target.id();
    },
    handleDragEnd(e) {
      this.isDragging = false;
      this.updateSelectedElement({
        x: e.target.x(),
        y: e.target.y()
      })
    },
    handleStageMouseDown(e) {
      // clicked on stage - clear selection
      if (e.target === e.target.getStage()) {
        this.selectedElementId = null;
        this.updateTransformer();
        return;
      }

      // clicked on transformer - do nothing
      if (e.target.getParent().className === 'Transformer') {
        return;
      }

      // find clicked element by its id
      const id = e.target.id();
      const element = this.getElementById(id);
      if (element) {
        this.selectedElementId = id;
      } else {
        this.selectedElementId = null;
      }
      this.updateTransformer();
    },

    updateTransformer() {
     
      
    },

    handleTransformEnd(e) {
      this.updateSelectedElement({
        x: e.target.x(),
        y: e.target.y(),
        rotation: e.target.rotation(),
        scaleX: e.target.scaleX(),
        scaleY: e.target.scaleY()
      })
    },

    fitStageIntoParentContainer() {
        var stage = this.$refs.stage.getNode()
        var container = this.$el.getBoundingClientRect()
        container.width = parseInt(container.width)
        container.height = parseInt(container.height)
        var scaleX = container.width / this.stageConfig.width;
        var scaleY = container.height / this.stageConfig.height;
        this.stageConfig.width = container.width
        this.stageConfig.height = container.height
        stage.width(container.width)
        stage.height(container.height)
        stage.scale({ x: scaleX, y: scaleY })
        stage.draw();
      }
  },
  mounted(){
    this.fitStageIntoParentContainer()
    window.addEventListener('resize', this.fitStageIntoParentContainer)
  },
  beforeDestroy(){
    window.removeEventListener('resize', this.fitStageIntoParentContainer)
  }
}