import EditElement from '@components/edit-element-text/edit-element-text.vue'

export default {
  props: ['index'],
  components: { EditElement },
  computed:{
    item(){
      return this.$store.state.editor.items[this.index]
    },
    elements(){
      return this.$store.getters['editor/getElementsByItemAndType']({item: this.index, type: 'text'})
    }
  },
  methods: {
    newTextPlaceholder(){
      return `Line ${this.elements.length + 1} (Max ${this.$store.state.editor.elementDefaults.text.maxCharacters} Characters)`
    },
    newElement() {
      this.$store.dispatch('editor/newTextElement', this.index)
    }
  }
}