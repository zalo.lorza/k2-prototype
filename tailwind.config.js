const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    preserveHtmlElements: true,
    enabled: process.env.NODE_ENV !== 'development',
    content: [
      './index.html', 
      './src/**/*.{vue,js,ts,jsx,tsx}',
      './src/js/vue-tailwind.js',
      'node_modules/vue-tailwind/dist/*.js'
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'sans': ['NB Akademie Std', 'ui-sans-serif', 'system-ui', 'sans-serif'],
      'compressed': ['GT America Trial Cn Md', 'ui-sans-serif', 'system-ui', 'sans-serif']
    },
    boxShadow: {
      DEFAULT: '0 0 10px 0px rgba(0, 0, 0, 0.2)',
    },
    extend: {
      colors: {
        gray: {
          100: '#EFEFEF',
          300: '#D8D8D8',
          700: '#808080',
          900: '#404040'
        }
      },
      minWidth: {
        4: '1rem',
        5: '1.25rem',
        6: '1.5rem',
        7: '1.75rem',
        8: '2rem',
        9: '2.25rem',
        10: '2.5rem',
        11: '2.75rem',
        12: '3rem',
        14: '3.5rem',
        16: '4rem',
        20: '5rem'
      },
      maxWidth: {
        4: '1rem',
        5: '1.25rem',
        6: '1.5rem',
        7: '1.75rem',
        8: '2rem',
        9: '2.25rem',
        10: '2.5rem',
        11: '2.75rem',
        12: '3rem',
        14: '3.5rem',
        15: '3.7rem',
        16: '4rem',
        20: '5rem'
      },
      minHeight: {
        4: '1rem',
        5: '1.25rem',
        6: '1.5rem',
        7: '1.75rem',
        8: '2rem',
        9: '2.25rem',
        10: '2.5rem',
        11: '2.75rem',
        12: '3rem',
        14: '3.5rem',
        16: '4rem',
        20: '5rem'
       }
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      cursor: ['disabled'],
      textDecoration: ['disabled'],
      backgroundColor: ['disabled']
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ]
}
